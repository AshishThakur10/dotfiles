#!/usr/bin/zsh

export LANG=en_US.UTF-8
export MANPATH="/usr/local/man:$MANPATH"
export ZSH=$HOME/.oh-my-zsh

COMPLETION_WAITING_DOTS="true"
DISABLE_AUTO_TITLE="true"
ENABLE_CORRECTION="false"
SPACESHIP_PROMPT_ASYNC=false
TERM=xterm-256color

zstyle ':omz:update' mode disabled
plugins=(
    git
    kubectx
#   zsh-autocomplete
    zsh-autosuggestions
    zsh-completions
    zsh-syntax-highlighting
)
autoload -U compinit && compinit
source $ZSH/oh-my-zsh.sh
eval "$(starship init zsh)"

source $HOME/.zsh_envs 2>/dev/null || true
source $HOME/.zsh_secrets 2>/dev/null || true
(cat $HOME/.path.list 2>/dev/null || true) |
    sed 's|\s*#\s*.*||g' |
    sed '/^[[:space:]]*$/d' |
    sed 's|\(.*\)|export PATH="\1:$PATH"|g' > \
        $HOME/.path.sh
source $HOME/.path.sh
source $HOME/.zsh_sources 2>/dev/null || true
source $HOME/.zsh_aliases 2>/dev/null || true
source $HOME/.zsh_functions 2>/dev/null || true
source $HOME/.zsh_secrets 2>/dev/null || true

autoload -U compinit && compinit

# fortune | cowsay | lolcat
echo " _________________________
< 🦇 FlyingFox Labs OS 🦇 >
 -------------------------
                    \\
              =/\    \\            /\\=
              / \\'._   (\_/)   _.'/ \\
             / .''._'--(o.o)--'_.''. \\
            /.' _/ |\`'=/ \" \\='\`| \\_ \`.\\
           /\` .' \`\\;-,'\\___/',-;/\` '. '\\
          /.-' jgs   \`\\(-V-)/\`       \`-.\\
          \`            \"   \"            \`
" | lolcat

# bun completions
. "$HOME/.asdf/asdf.sh"
